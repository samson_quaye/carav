<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/28/2016
 * Time: 1:52 AM
 */

namespace AppBundle\Entity;


use AppBundle\Model\Metadata;
use AppBundle\Model\MetadataInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class User
 * @package AppBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable, MetadataInterface
{

  use Metadata;

  /**
   * @var string
   *
   * @ORM\Id()
   * @ORM\Column(type="string", length=64)
   * @ORM\GeneratedValue(strategy="UUID")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=15)
   * @NotBlank()
   * @Length(max="15", min="5")
   */
  private $username;

  /**
   * @var string
   * @NotBlank()
   * @Length(min="5")
   */
  private $plainPassword;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=128)
   */
  private $password;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=90)
   * @NotBlank()
   * @Length(max="90")
   */
  private $fullName;

  /**
   * @var string
   *
   * @ORM\Column(type="json_array")
   */
  private $roles;

  /**
   * @return string
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Returns the roles granted to the user.
   *
   * <code>
   * public function getRoles()
   * {
   *     return array('ROLE_USER');
   * }
   * </code>
   *
   * Alternatively, the roles might be stored on a ``roles`` property,
   * and populated in any number of different ways when the user object
   * is created.
   *
   * @return (Role|string)[] The user roles
   */
  public function getRoles()
  {
    return $this->roles;
  }

  /**
   * Returns the password used to authenticate the user.
   *
   * This should be the encoded password. On authentication, a plain-text
   * password will be salted, encoded, and then compared to this value.
   *
   * @return string The password
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * @param $password
   */
  public function setPassword($password)
  {
    $this->password = $password;
  }

  /**
   * Returns the salt that was originally used to encode the password.
   *
   * This can return null if the password was not encoded using a salt.
   *
   * @return string|null The salt
   */
  public function getSalt()
  {
    return null;
  }

  /**
   * Returns the username used to authenticate the user.
   *
   * @return string The username
   */
  public function getUsername()
  {
    return $this->username;
  }

  /**
   * @param $fullName
   */
  public function setFullName($fullName)
  {
    $this->fullName = $fullName;
  }

  /**
   * @return string
   */
  public function getFullName()
  {
    return $this->fullName;
  }

  /**
   * Removes sensitive data from the user.
   *
   * This is important if, at any given point, sensitive information like
   * the plain-text password is stored on this object.
   */
  public function eraseCredentials()
  {
    // TODO: Implement eraseCredentials() method.
  }

  /**
   * (PHP 5 &gt;= 5.1.0)<br/>
   * String representation of object
   * @link http://php.net/manual/en/serializable.serialize.php
   * @return string the string representation of the object or null
   */
  public function serialize()
  {
    return serialize(array(
      $this->id,
      $this->username,
      $this->password,
      // see section on salt below
      // $this->salt,
    ));
  }

  /**
   * (PHP 5 &gt;= 5.1.0)<br/>
   * Constructs the object
   * @link http://php.net/manual/en/serializable.unserialize.php
   * @param string $serialized <p>
   * The string representation of the object.
   * </p>
   * @return void
   */
  public function unserialize($serialized)
  {
    list (
      $this->id,
      $this->username,
      $this->password,
      // see section on salt below
      // $this->salt
      ) = unserialize($serialized);
  }

  /**
   * @return string
   */
  public function getPlainPassword()
  {
    return $this->plainPassword;
  }

  /**
   * @param string $plainPassword
   */
  public function setPlainPassword($plainPassword)
  {
    $this->plainPassword = $plainPassword;
  }

  /**
   * @param $roles
   */
  public function setRoles($roles)
  {
    $this->roles = array($roles);
  }

  /**
   * @param string $id
   */
  public function setId($id)
  {
    $this->id = $id;
  }

  /**
   * @param string $username
   */
  public function setUsername($username)
  {
    $this->username = $username;
  }
}