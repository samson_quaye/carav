<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/26/2016
 * Time: 11:21 PM
 */

namespace AppBundle\Entity;


use AppBundle\Model\MetadataInterface;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Class Victim
 * @package AppBundle\Entity
 *
 * @Entity()
 */
class Victim extends Person
{

  /**
   * @var LegalCase
   *
   * @OneToOne(targetEntity="AppBundle\Entity\LegalCase", inversedBy="victim", cascade={"persist"})
   */
  private $legalCase;

  /**
   * @return LegalCase
   */
  public function getLegalCase()
  {
    return $this->legalCase;
  }

  /**
   * @param LegalCase $legalCase
   */
  public function setLegalCase($legalCase)
  {
    $this->legalCase = $legalCase;
  }
}
