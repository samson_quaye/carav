<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 9/1/2016
 * Time: 1:28 AM
 */

namespace AppBundle\Entity;


use AppBundle\Model\MetadataInterface;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Class Complainant
 * @package AppBundle\Entity
 *
 * @Entity(repositoryClass="AppBundle\Repository\ComplaintRepository")
 */
class Complainant extends Person
{

  /**
   * @var LegalCase
   *
   * @OneToOne(targetEntity="AppBundle\Entity\LegalCase", inversedBy="complainant", cascade={"persist"})
   */
  private $legalCase;

  /**
   * @return LegalCase
   */
  public function getLegalCase()
  {
    return $this->legalCase;
  }

  /**
   * @param LegalCase $legalCase
   */
  public function setLegalCase($legalCase)
  {
    $this->legalCase = $legalCase;
  }


}