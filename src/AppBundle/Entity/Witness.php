<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/26/2016
 * Time: 11:24 PM
 */

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * Class Witness
 * @package AppBundle\Entity
 *
 * @Entity()
 */
class Witness extends Person
{

  /**
   * @var LegalCase
   *
   * @OneToOne(targetEntity="AppBundle\Entity\LegalCase", inversedBy="witness", cascade={"persist"})
   */
  private $legalCase;

  /**
   * @return LegalCase
   */
  public function getLegalCase()
  {
    return $this->legalCase;
  }

  /**
   * @param LegalCase $legalCase
   */
  public function setLegalCase($legalCase)
  {
    $this->legalCase = $legalCase;
  }
}
