<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/26/2016
 * Time: 6:13 PM
 */

namespace AppBundle\Entity;


use AppBundle\Model\Metadata;
use AppBundle\Model\MetadataInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Person
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ComplaintRepository")
 * @ORM\Table(name="person")
 * @ORM\InheritanceType(value="SINGLE_TABLE")
 * @ORM\DiscriminatorMap(value={"Victim" = "Victim", "Complainant" = "Complainant", "Suspect" = "Suspect", "Witness" = "Witness"})
 * @ORM\DiscriminatorColumn(type="string", fieldName="type", name="type")
 */
abstract class Person implements MetadataInterface
{

  use Metadata;

  /**
   * @var string
   *
   * @ORM\Id()
   * @ORM\GeneratedValue(strategy="UUID")
   * @ORM\Column(type="string", length=64)
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=150)
   * @Assert\Length(min="4", max="150")
   * @Assert\NotBlank()
   *
   */
  private $fullName;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   * @Assert\NotBlank()
   */
  private $address;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=30)
   * @Assert\NotBlank()
   * @Assert\Length(max="30", min="3")
   */
  private $ward;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=30)
   * @Assert\NotBlank()
   * @Assert\Length(max="30", min="3")
   */
  private $lga;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   * @Assert\NotBlank()
   */
  private $placeOfWork;

  /**
   * @var string
   *
   * @ORM\Column(type="string", nullable=true)
   */
  private $occupation;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=15)
   * @Assert\Length(min="6", max="15")
   */
  private $phone;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=150)
   * @Assert\Email()
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=15, nullable=true)
   * @Assert\NotBlank()
   */
  private $relationshipWithVictim;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=15, nullable=true)
   * @Assert\NotBlank()
   */
  private $relationshipWithSuspect;

  /**
   * Get id
   *
   * @return string
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set fullName
   *
   * @param string $fullName
   *
   * @return Complainant
   */
  public function setFullName($fullName)
  {
    $this->fullName = $fullName;

    return $this;
  }

  /**
   * Get fullName
   *
   * @return string
   */
  public function getFullName()
  {
    return $this->fullName;
  }

  /**
   * Set address
   *
   * @param string $address
   *
   * @return Complainant
   */
  public function setAddress($address)
  {
    $this->address = $address;

    return $this;
  }

  /**
   * Get address
   *
   * @return string
   */
  public function getAddress()
  {
    return $this->address;
  }

  /**
   * Set ward
   *
   * @param string $ward
   *
   * @return Complainant
   */
  public function setWard($ward)
  {
    $this->ward = $ward;

    return $this;
  }

  /**
   * Get ward
   *
   * @return string
   */
  public function getWard()
  {
    return $this->ward;
  }

  /**
   * Set lga
   *
   * @param string $lga
   *
   * @return Complainant
   */
  public function setLga($lga)
  {
    $this->lga = $lga;

    return $this;
  }

  /**
   * Get lga
   *
   * @return string
   */
  public function getLga()
  {
    return $this->lga;
  }

  /**
   * Set placeOfWork
   *
   * @param string $placeOfWork
   *
   * @return Complainant
   */
  public function setPlaceOfWork($placeOfWork)
  {
    $this->placeOfWork = $placeOfWork;

    return $this;
  }

  /**
   * Get placeOfWork
   *
   * @return string
   */
  public function getPlaceOfWork()
  {
    return $this->placeOfWork;
  }

  /**
   * Set phone
   *
   * @param string $phone
   *
   * @return Complainant
   */
  public function setPhone($phone)
  {
    $this->phone = $phone;

    return $this;
  }

  /**
   * Get phone
   *
   * @return string
   */
  public function getPhone()
  {
    return $this->phone;
  }

  /**
   * Set email
   *
   * @param string $email
   *
   * @return Complainant
   */
  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  /**
   * Get email
   *
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * Set relationshipWithVictim
   *
   * @param string $relationshipWithVictim
   *
   * @return Complainant
   */
  public function setRelationshipWithVictim($relationshipWithVictim)
  {
    $this->relationshipWithVictim = $relationshipWithVictim;

    return $this;
  }

  /**
   * Get relationshipWithVictim
   *
   * @return string
   */
  public function getRelationshipWithVictim()
  {
    return $this->relationshipWithVictim;
  }

  /**
   * Set relationshipWithSuspect
   *
   * @param string $relationshipWithSuspect
   *
   * @return Complainant
   */
  public function setRelationshipWithSuspect($relationshipWithSuspect)
  {
    $this->relationshipWithSuspect = $relationshipWithSuspect;

    return $this;
  }

  /**
   * Get relationshipWithSuspect
   *
   * @return string
   */
  public function getRelationshipWithSuspect()
  {
    return $this->relationshipWithSuspect;
  }

  /**
   * @return string
   */
  public function getOccupation()
  {
    return $this->occupation;
  }

  /**
   * @param string $occupation
   */
  public function setOccupation($occupation)
  {
    $this->occupation = $occupation;
  }
}
