<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/26/2016
 * Time: 6:11 PM
 */

namespace AppBundle\Entity;


use AppBundle\Entity\Complainant;
use AppBundle\Entity\Suspect;
use AppBundle\Entity\Victim;
use AppBundle\Entity\Witness;
use AppBundle\Model\Metadata;
use AppBundle\Model\MetadataInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class LegalCase
 * @package AppBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LegalCase")
 * @UniqueEntity(fields={"caseNumber", "caseName"})
 */
class LegalCase implements MetadataInterface
{

  use Metadata;

  /**
   * @var string
   *
   * @ORM\Id()
   * @ORM\GeneratedValue(strategy="UUID")
   * @ORM\Column(length=64, type="string")
   */
  private $id;

  /**
   * @var Complainant
   *
   * @ORM\OneToOne(targetEntity="AppBundle\Entity\Complainant", mappedBy="legalCase", cascade={"persist"})
   */
  private $complainant;

  /**
   * @var Victim
   *
   * @ORM\OneToOne(targetEntity="AppBundle\Entity\Victim", mappedBy="legalCase", cascade={"persist"})
   */
  private $victim;

  /**
   * @var Witness
   *
   * @ORM\OneToOne(targetEntity="AppBundle\Entity\Witness", mappedBy="legalCase", cascade={"persist"})
   */
  private $witness;

  /**
   * @var Suspect
   *
   * @ORM\OneToOne(targetEntity="AppBundle\Entity\Suspect", mappedBy="legalCase", cascade={"persist"})
   */
  private $suspect;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=150)
   * @Assert\Length(max="150", min="10")
   * @Assert\NotBlank()
   */
  private $caseNumber;

  /**
   * @var string
   *
   * @ORM\Column(type="string", length=150)
   * @Assert\NotBlank()
   * @Assert\Length(max="150", min="10")
   */
  private $caseName;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   * @Assert\NotBlank()
   */
  private $crimeScene;

  /**
   * @var \DateTime
   *
   * @ORM\Column(type="datetime")
   * @Assert\NotBlank()
   */
  private $incidentDate;

  /**
   * @var \DateTime
   *
   * @ORM\Column(type="datetime")
   * @Assert\NotBlank()
   */
  private $complainDate;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $relevantInformation;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $actionTakenBeforeComplain;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $recommendation;

  /**
   * @return string
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Set caseNumber
   *
   * @param string $caseNumber
   *
   * @return LegalCase
   */
  public function setCaseNumber($caseNumber)
  {
    $this->caseNumber = $caseNumber;

    return $this;
  }

  /**
   * Get caseNumber
   *
   * @return string
   */
  public function getCaseNumber()
  {
    return $this->caseNumber;
  }

  /**
   * Set caseName
   *
   * @param string $caseName
   *
   * @return LegalCase
   */
  public function setCaseName($caseName)
  {
    $this->caseName = $caseName;

    return $this;
  }

  /**
   * Get caseName
   *
   * @return string
   */
  public function getCaseName()
  {
    return $this->caseName;
  }

  /**
   * Set crimeScene
   *
   * @param string $crimeScene
   *
   * @return LegalCase
   */
  public function setCrimeScene($crimeScene)
  {
    $this->crimeScene = $crimeScene;

    return $this;
  }

  /**
   * Get crimeScene
   *
   * @return string
   */
  public function getCrimeScene()
  {
    return $this->crimeScene;
  }

  /**
   * Set incidentDate
   *
   * @param \DateTime $incidentDate
   *
   * @return LegalCase
   */
  public function setIncidentDate(\DateTime $incidentDate)
  {
    $this->incidentDate = $incidentDate;

    return $this;
  }

  /**
   * Get incidentDate
   *
   * @return \DateTime
   */
  public function getIncidentDate()
  {
    return $this->incidentDate;
  }

  /**
   * Set complainDate
   *
   * @param \DateTime $complainDate
   *
   * @return LegalCase
   */
  public function setComplainDate(\DateTime $complainDate)
  {
    $this->complainDate = $complainDate;

    return $this;
  }

  /**
   * Get complainDate
   *
   * @return \DateTime
   */
  public function getComplainDate()
  {
    return $this->complainDate;
  }

  /**
   * Set relevantInformation
   *
   * @param string $relevantInformation
   *
   * @return LegalCase
   */
  public function setRelevantInformation($relevantInformation)
  {
    $this->relevantInformation = $relevantInformation;

    return $this;
  }

  /**
   * Get relevantInformation
   *
   * @return string
   */
  public function getRelevantInformation()
  {
    return $this->relevantInformation;
  }

  /**
   * Set actionTakenBeforeComplain
   *
   * @param string $actionTakenBeforeComplain
   *
   * @return LegalCase
   */
  public function setActionTakenBeforeComplain($actionTakenBeforeComplain)
  {
    $this->actionTakenBeforeComplain = $actionTakenBeforeComplain;

    return $this;
  }

  /**
   * Get actionTakenBeforeComplain
   *
   * @return string
   */
  public function getActionTakenBeforeComplain()
  {
    return $this->actionTakenBeforeComplain;
  }

  /**
   * Set recommendation
   *
   * @param string $recommendation
   *
   * @return LegalCase
   */
  public function setRecommendation($recommendation)
  {
    $this->recommendation = $recommendation;

    return $this;
  }

  /**
   * Get recommendation
   *
   * @return string
   */
  public function getRecommendation()
  {
    return $this->recommendation;
  }

  /**
   * Set complainant
   *
   * @param Complainant $complainant
   *
   * @return LegalCase
   */
  public function setComplainant(Complainant $complainant = null)
  {
    $this->complainant = $complainant;
    $complainant->setLegalCase($this);

    return $this;
  }

  /**
   * Get complainant
   *
   * @return Complainant
   */
  public function getComplainant()
  {
    return $this->complainant;
  }

  /**
   * Set victim
   *
   * @param Victim $victim
   *
   * @return LegalCase
   */
  public function setVictim(Victim $victim = null)
  {
    $this->victim = $victim;
    $victim->setLegalCase($this);

    return $this;
  }

  /**
   * Get victim
   *
   * @return Victim
   */
  public function getVictim()
  {
    return $this->victim;
  }

  /**
   * Set witness
   *
   * @param Witness $witness
   *
   * @return LegalCase
   */
  public function setWitness(Witness $witness = null)
  {
    $this->witness = $witness;
    $witness->setLegalCase($this);

    return $this;
  }

  /**
   * Get witness
   *
   * @return Witness
   */
  public function getWitness()
  {
    return $this->witness;
  }

  /**
   * Set suspect
   *
   * @param Suspect $suspect
   *
   * @return LegalCase
   */
  public function setSuspect(Suspect $suspect = null)
  {
    $this->suspect = $suspect;
    $suspect->setLegalCase($this);

    return $this;
  }

  /**
   * Get suspect
   *
   * @return Suspect
   */
  public function getSuspect()
  {
    return $this->suspect;
  }
}
