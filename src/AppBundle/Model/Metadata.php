<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/26/2016
 * Time: 6:03 PM
 */

namespace AppBundle\Model;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Metadata
 * @package AppBundle\Model
 */
trait Metadata
{

  /**
   * @var \DateTime
   *
   * @ORM\Column(type="datetime")
   */
  private $createdOn;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $createdBy;

  /**
   * @var \DateTime
   *
   * @ORM\Column(type="datetime")
   */
  private $updatedOn;

  /**
   * @var string
   *
   * @ORM\Column(type="string")
   */
  private $updatedBy;

  /**
   * @return mixed
   */
  public function getCreatedOn()
  {
    return $this->createdOn;
  }

  /**
   * @param mixed $createdOn
   */
  public function setCreatedOn(\DateTime $createdOn)
  {
    $this->createdOn = $createdOn;
  }

  /**
   * @return mixed
   */
  public function getCreatedBy()
  {
    return $this->createdBy;
  }

  /**
   * @param mixed $createdBy
   */
  public function setCreatedBy($createdBy)
  {
    $this->createdBy = $createdBy;
  }

  /**
   * @return mixed
   */
  public function getUpdatedOn()
  {
    return $this->updatedOn;
  }

  /**
   * @param mixed $updatedOn
   */
  public function setUpdatedOn(\DateTime $updatedOn)
  {
    $this->updatedOn = $updatedOn;
  }

  /**
   * @return mixed
   */
  public function getUpdatedBy()
  {
    return $this->updatedBy;
  }

  /**
   * @param mixed $updatedBy
   */
  public function setUpdatedBy($updatedBy)
  {
    $this->updatedBy = $updatedBy;
  }
}