<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 9/1/2016
 * Time: 10:28 AM
 */

namespace AppBundle\Model;


interface MetadataInterface
{

  public function setCreatedOn(\DateTime $date);

  public function setCreatedBy($userId);

  public function setUpdatedBy($userId);

  public function setUpdatedOn(\DateTime $date);
}