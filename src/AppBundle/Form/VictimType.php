<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/31/2016
 * Time: 6:16 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VictimType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('fullName')
      ->add('address', TextareaType::class)
      ->add('ward')
      ->add('lga')
      ->add('phone')
      ->add('email')
      ->add('relationshipWithSuspect')
      ->add('occupation');
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'AppBundle\Entity\Victim'
    ));
  }
}