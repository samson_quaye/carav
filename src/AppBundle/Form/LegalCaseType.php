<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LegalCaseType extends AbstractType
{
  /**
   * @param FormBuilderInterface $builder
   * @param array $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('caseNumber')
      ->add('caseName')
      ->add('crimeScene')
      ->add('incidentDate', DateType::class, [
        'widget' => 'single_text'
      ])
      ->add('complainDate', DateType::class, [
        'widget' => 'single_text'
      ])
      ->add('complainant', ComplainantType::class)
      ->add('victim', VictimType::class)
      ->add('witness', WitnessType::class)
      ->add('suspect', SuspectType::class)
      ->add('relevantInformation', TextareaType::class)
      ->add('actionTakenBeforeComplain', TextareaType::class)
      ->add('recommendation', TextareaType::class);

//    $builder->get('incidentDate')->addModelTransformer(new CallbackTransformer(function ($value) {
//      return date_format($value, 'm-d-Y');
//    }, function ($value) {
//      return new \DateTime($value);
//    }));
//
//    $builder->get('complainDate')->addModelTransformer(new CallbackTransformer(function ($value) {
//      return date_format($value, 'm-d-Y');
//    }, function ($value) {
//      return new \DateTime($value);
//    }));
  }

  /**
   * @param OptionsResolver $resolver
   */
  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'AppBundle\Entity\LegalCase'
    ));
  }
}
