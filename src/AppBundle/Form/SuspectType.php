<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/31/2016
 * Time: 6:16 PM
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SuspectType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('fullName')
      ->add('address')
      ->add('ward')
      ->add('lga')
      ->add('placeOfWork')
      ->add('phone')
      ->add('email')
      ->add('relationshipWithVictim');
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => 'AppBundle\Entity\Suspect'
    ));
  }
}