<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 9/1/2016
 * Time: 10:09 AM
 */

namespace AppBundle\EventListener;


use AppBundle\Model\MetadataInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MetadataListener implements EventSubscriber
{

  /**
   * @var TokenStorageInterface
   */
  private $tokenStorage;

  public function __construct(TokenStorageInterface $tokenStorage)
  {
    $this->tokenStorage = $tokenStorage;
  }

  public function prePersist(LifecycleEventArgs $event)
  {
    /** @var MetadataInterface $entity */
    $entity = $event->getEntity();

    if (!$entity instanceof MetadataInterface) {
      return;
    }

    $userId = $this->tokenStorage->getToken()->getUser()->getId();

    $this->addCreatedAtMetadata($userId, $entity);
    $this->addUpdatedAtMetadata($userId, $entity);
  }

  public function preUpdate(LifecycleEventArgs $event)
  {
    /** @var MetadataInterface $entity */
    $entity = $event->getEntity();

    if (!$entity instanceof MetadataInterface) {
      return;
    }

    $userId = $this->tokenStorage->getToken()->getUser()->getId();

    $this->addUpdatedAtMetadata($userId, $entity);
  }

  private function addCreatedAtMetadata($userId, MetadataInterface $entity)
  {
    $entity->setCreatedBy($userId);
    $entity->setCreatedOn(new \DateTime());
  }

  private function addUpdatedAtMetadata($userId, MetadataInterface $entity)
  {
    $entity->setUpdatedBy($userId);
    $entity->setUpdatedOn(new \DateTime());
  }

  /**
   * Returns an array of events this subscriber wants to listen to.
   *
   * @return array
   */
  public function getSubscribedEvents()
  {
    return [
      Events::prePersist,
      Events::preUpdate
    ];
  }
}