<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 9/1/2016
 * Time: 1:59 AM
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class RoleAssigner
 * @package AppBundle\EventListener
 */
class RoleAssigner
{

  public function prePersist(LifecycleEventArgs $event)
  {
    /** @var User $entity */
    $entity = $event->getEntity();
    $em = $event->getEntityManager();

    if (!$entity instanceof UserInterface) {
      return;
    }

    $userCount = $em->getRepository('AppBundle:User')->countUser();

    if (!$userCount) {
      $entity->setRoles('ROLE_ADMIN');
    } else {
      $entity->setRoles('ROLE_USER');
    }
  }
}