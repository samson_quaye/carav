<?php
/**
 * Created by PhpStorm.
 * User: SNAQuaye
 * Date: 8/30/2016
 * Time: 9:23 AM
 */

namespace AppBundle\Controller;


use AppBundle\Model\PreAuthFilter;
use AppBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller implements PreAuthFilter
{

  /**
   * @param Request $request
   *
   * @Route(path="/login", name="login")
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function loginAction(Request $request)
  {
    $authenticationUtils = $this->get('security.authentication_utils');

    // get the login error if there is one
    $error = $authenticationUtils->getLastAuthenticationError();

    // last username entered by the user
    $lastUsername = $authenticationUtils->getLastUsername();

    return $this->render(
      'security/login.html.twig',
      array(
        // last username entered by the user
        'last_username' => $lastUsername,
        'error'         => $error,
      )
    );
  }
}