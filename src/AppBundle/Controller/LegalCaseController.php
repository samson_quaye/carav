<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Complainant;
use AppBundle\Entity\Suspect;
use AppBundle\Entity\Victim;
use AppBundle\Entity\Witness;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\LegalCase;
use AppBundle\Form\LegalCaseType;

/**
 * LegalCase controller.
 *
 * @Route("/legalcase")
 */
class LegalCaseController extends Controller
{
  /**
   * Lists all LegalCase entities.
   *
   * @Route("/", name="legalcase_index")
   * @Method("GET")
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $legalCases = $em->getRepository('AppBundle:LegalCase')->findAll();

    return $this->render('legalcase/index.html.twig', array(
      'legalCases' => $legalCases,
      'pageTitle' => 'Case List'
    ));
  }

  /**
   * Creates a new LegalCase entity.
   *
   * @Route("/new", name="legalcase_new")
   * @Method({"GET", "POST"})
   * @param Request $request
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   */
  public function newAction(Request $request)
  {
//    dump($request->request);exit;
    $legalCase = new LegalCase();
    $suspect = new Suspect();
    $witness = new Witness();
    $complainant = new Complainant();
    $victim = new Victim();

    $legalCase->setSuspect($suspect);
    $legalCase->setWitness($witness);
    $legalCase->setComplainant($complainant);
    $legalCase->setVictim($victim);

    $form = $this->createForm('AppBundle\Form\LegalCaseType', $legalCase);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($legalCase);
      $em->flush();

      return $this->redirectToRoute('legalcase_show', array('id' => $legalCase->getId()));
    }

    return $this->render('legalcase/new.html.twig', array(
      'legalCase' => $legalCase,
      'form' => $form->createView(),
      'pageTitle' => 'New Case'
    ));
  }

  /**
   * Finds and displays a LegalCase entity.
   *
   * @Route("/{id}", name="legalcase_show")
   * @Method("GET")
   */
  public function showAction(LegalCase $legalCase)
  {
    $deleteForm = $this->createDeleteForm($legalCase);

    return $this->render('legalcase/show.html.twig', array(
      'legalCase' => $legalCase,
      'delete_form' => $deleteForm->createView(),
      'pageTitle' => 'Detail for Case Number: '.$legalCase->getCaseNumber()
    ));
  }

  /**
   * Displays a form to edit an existing LegalCase entity.
   *
   * @Route("/{id}/edit", name="legalcase_edit")
   * @Method({"GET", "POST"})
   */
  public function editAction(Request $request, LegalCase $legalCase)
  {
    $deleteForm = $this->createDeleteForm($legalCase);
    $editForm = $this->createForm('AppBundle\Form\LegalCaseType', $legalCase);
    $editForm->handleRequest($request);

    if ($editForm->isSubmitted() && $editForm->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($legalCase);
      $em->flush();

      return $this->redirectToRoute('legalcase_edit', array('id' => $legalCase->getId()));
    }

    return $this->render('legalcase/edit.html.twig', array(
      'legalCase' => $legalCase,
      'edit_form' => $editForm->createView(),
      'delete_form' => $deleteForm->createView(),
      'pageTitle' => 'Case Number: '.$legalCase->getCaseNumber()
    ));
  }

  /**
   * Deletes a LegalCase entity.
   *
   * @Route("/{id}", name="legalcase_delete")
   * @Method("DELETE")
   */
  public function deleteAction(Request $request, LegalCase $legalCase)
  {
    $form = $this->createDeleteForm($legalCase);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->remove($legalCase);
      $em->flush();
    }

    return $this->redirectToRoute('legalcase_index');
  }

  /**
   * Creates a form to delete a LegalCase entity.
   *
   * @param LegalCase $legalCase The LegalCase entity
   *
   * @return \Symfony\Component\Form\Form The form
   */
  private function createDeleteForm(LegalCase $legalCase)
  {
    return $this->createFormBuilder()
      ->setAction($this->generateUrl('legalcase_delete', array('id' => $legalCase->getId())))
      ->setMethod('DELETE')
      ->getForm();
  }
}
