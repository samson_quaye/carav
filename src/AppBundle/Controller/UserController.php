<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends Controller
{
  /**
   * Lists all User entities.
   *
   * @Route("/", name="user_index")
   * @Security("is_granted('ROLE_ADMIN')")
   * @Method("GET")
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $users = $em->getRepository('AppBundle:User')->findAll();

    return $this->render('user/index.html.twig', array(
      'users' => $users,
      'pageTitle' => 'User List'
    ));
  }

  /**
   * Creates a new User entity.
   *
   * @Route("/new", name="user_new")
   * @Security("is_granted('ROLE_ADMIN')")
   * @Method({"GET", "POST"})
   */
  public function newAction(Request $request)
  {
    $user = new User();
    $form = $this->createForm('AppBundle\Form\UserType', $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $password = $this->get('security.password_encoder')
        ->encodePassword($user, $user->getPlainPassword());
      $user->setPassword($password);

      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();

      return $this->redirectToRoute('user_show', array('id' => $user->getId()));
    }

    return $this->render('user/new.html.twig', array(
      'user' => $user,
      'form' => $form->createView(),
      'pageTitle' => 'Create User Account'
    ));
  }

  /**
   * Finds and displays a User entity.
   *
   * @Route("/{id}", name="user_show")
   * @Security("is_granted('ROLE_ADMIN')")
   * @Method("GET")
   */
  public function showAction(User $user)
  {
    $deleteForm = $this->createDeleteForm($user);

    return $this->render('user/show.html.twig', array(
      'user' => $user,
      'delete_form' => $deleteForm->createView(),
      'pageTitle' => sprintf('Detail for %s', $user->getFullName())
    ));
  }

  /**
   * Displays a form to edit an existing User entity.
   *
   * @Route("/{id}/edit", name="user_edit")
   * @Security("is_granted('ROLE_ADMIN')")
   * @Method({"GET", "POST"})
   */
  public function editAction(Request $request, User $user)
  {
    $deleteForm = $this->createDeleteForm($user);
    $editForm = $this->createForm('AppBundle\Form\UserType', $user);
    $editForm->handleRequest($request);

    if ($editForm->isSubmitted() && $editForm->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();

      return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
    }

    return $this->render('user/edit.html.twig', array(
      'user' => $user,
      'edit_form' => $editForm->createView(),
      'delete_form' => $deleteForm->createView(),
      'pageTitle' => sprintf('Update %s Account', $user->getFullName()),
    ));
  }

  /**
   * Deletes a User entity.
   *
   * @Route("/{id}", name="user_delete")
   * @Security("is_granted('ROLE_ADMIN')")
   * @Method("DELETE")
   */
  public function deleteAction(Request $request, User $user)
  {
    $form = $this->createDeleteForm($user);
    $form->handleRequest($request);

    if ($user->getRoles() === ['ROLE_ADMIN']) {
      throw new BadRequestHttpException("Admin account can't be deleted");
    }

    if ($form->isSubmitted() && $form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->remove($user);
      $em->flush();
    }

    return $this->redirectToRoute('user_index');
  }

  /**
   * Creates a form to delete a User entity.
   *
   * @param User $user The User entity
   *
   * @return \Symfony\Component\Form\Form The form
   */
  private function createDeleteForm(User $user)
  {
    return $this->createFormBuilder()
      ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
      ->setMethod('DELETE')
      ->getForm();
  }
}
